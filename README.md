# Jekyll - Branched

All you can find here is this README file in the `main` branch. GitLab Pages
live in the [`pages` branch](https://gitlab.com/pages/jekyll-branched/-/tree/pages).

This way you can have your project's code in the `main` branch and use an
orphan branch (`pages`) for hosting your site. Learn more about it in the
[documentation].

View the Pages source content: https://gitlab.com/pages/jekyll-branched/tree/pages

View site: https://pages.gitlab.io/jekyll-branched/

[documentation]: https://docs.gitlab.com/ee/user/project/pages/introduction.html#gitlab-ciyml-for-a-repository-where-theres-also-actual-code
